from flask import Flask
from flask import render_template
from flask import request

from config import API_KEY
import gb_api

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('search.html')

@app.route('/search')
def search():
    query = request.args.get('query')
    results = gb_api.search(query)
    return render_template('search.html', results=results)

@app.route('/checkout', methods=['POST'])
def checkout():
    game_ids = request.form.getlist('selected')
    game_info = gb_api.get_data_for_ids(game_ids)
    return render_template('checkout.html', game_info=game_info)


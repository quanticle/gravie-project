# Gravie Game Library

## Setup
* `pip install -r requirements.txt`
* Add your GiantBomb API key to `config.py`
* `flask run`

## To run unit tests:
* `python -m pytest tests`



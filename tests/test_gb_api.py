from unittest.mock import (
    call,
    MagicMock,
    patch
)

import gb_api

@patch('gb_api.config')
@patch('gb_api.requests')
def test_gb_search_single_result(mock_requests, mock_config):
    mock_config.API_KEY = "test api key"
    mock_response = MagicMock()
    mock_response.json.side_effect = [{
        'number_of_total_results': 1,
        'number_of_page_results': 1,
        'results': [
            {
                'guid': 'test-guid',
                'name': 'test game',
                'image': {
                    'thumb_url': 'thumb_url'
                }
            }
        ]
    }]
    mock_requests.get.side_effect = [mock_response]

    results = gb_api.search("test")

    assert results == [
        {
            'id': 'test-guid',
            'name': 'test game',
            'image_url': 'thumb_url'
        }
    ]

    assert mock_requests.get.mock_calls == [
            call('https://www.giantbomb.com/api/search/', 
                headers={'user-agent': 'gb_test'}, 
                params={
                    'api_key': 'test api key', 
                    'format': 'json', 
                    'field_list': 'name,image,guid', 
                    'query': 'test', 
                    'resources': 'game',
                    'page': 1})]

@patch('gb_api.config')
@patch('gb_api.requests')
def test_gb_search_multiple_results(mock_requests, mock_config):
    mock_config.API_KEY = "test api key"
    mock_response = MagicMock()
    mock_response.json.side_effect = [{
        'number_of_total_results': 1,
        'number_of_page_results': 1,
        'results': [
            {
                'guid': 'test-guid-1',
                'name': 'test game 1',
                'image': {
                    'thumb_url': 'thumb_url_1'
                }
            },
            {
                'guid': 'test-guid-2',
                'name': 'test game 2',
                'image': {
                    'thumb_url': 'thumb_url_2'
                }
            }
        ]
    }]

    mock_requests.get.side_effect = [mock_response]

    results = gb_api.search("test")

    assert results == [
        {
            "id": "test-guid-1",
            "name": "test game 1",
            "image_url": "thumb_url_1"
        },
        {
            "id": "test-guid-2",
            "name": "test game 2",
            "image_url": "thumb_url_2"
        }
    ]

    assert mock_requests.get.mock_calls == [
            call('https://www.giantbomb.com/api/search/', 
                headers={'user-agent': 'gb_test'}, 
                params={
                    'api_key': 'test api key', 
                    'format': 'json', 
                    'field_list': 'name,image,guid', 
                    'query': 'test', 
                    'resources': 'game',
                    'page': 1})]


@patch('gb_api.config')
@patch('gb_api.requests')
def test_gb_search_pagination(mock_requests, mock_config):
    mock_config.API_KEY = "test api key"
    mock_response = MagicMock()
    mock_response.json.side_effect = [
        {
            'offset': 0,
            'number_of_page_results': 1,
            'number_of_total_results': 2,
            'results': [
                {
                    'guid': 'test-guid-1',
                    'name': 'test game 1',
                    'image': {
                        'thumb_url': 'test_thumb_1'
                    }
                },
            ]
        },
        {
            'offset': 1,
            'number_of_page_results': 1,
            'number_of_total_results': 2,
            'results': [
                {
                    'guid': 'test-guid-2',
                    'name': 'test game 2',
                    'image': {
                        'thumb_url': 'test_thumb_2'
                    }
                }
            ]
        }
    ]

    mock_requests.get.side_effect = [mock_response, mock_response]

    results = gb_api.search("test")
    
    assert results == [
        {
            "id": "test-guid-1",
            "name": "test game 1",
            "image_url": "test_thumb_1"
        },
        {
            "id": "test-guid-2",
            "name": "test game 2",
            "image_url": "test_thumb_2"
        }
    ]

@patch('gb_api.config')
@patch('gb_api.requests')
def test_get_game_info_single_game(mock_requests, mock_config):
    mock_config.API_KEY = "test api key"
    mock_response = MagicMock()
    mock_response.json.side_effect = [
        {
            "results": {
                "name": "test game",
                "image": {
                    "thumb_url": "test_thumb_url"
                }
            }
        }
    ]
    mock_requests.get.side_effect = [mock_response]

    game_data = gb_api.get_data_for_ids(["test_id"])

    assert game_data == [
        {
            "name": "test game",
            "image_url": "test_thumb_url"
        }
    ]
    print(mock_requests.get.mock_calls)
    assert mock_requests.get.mock_calls == [call('https://www.giantbomb.com/api/game/test_id', 
        headers={'user-agent': 'gb-test'}, 
        params={'api_key': 'test api key', 'format': 'json', 'field_list': 'name,image'})]


@patch('gb_api.config')
@patch('gb_api.requests')
def test_get_game_info_multiple_games(mock_requests, mock_config):
    mock_config.API_KEY = "test api key"
    mock_response_1 = MagicMock()
    mock_response_1.json.side_effect = [
        {
            "results": {
                "name": "test game",
                "image": {
                    "thumb_url": "test_thumb_url"
                }
            }
        }
    ]
    mock_response_2 = MagicMock()
    mock_response_2.json.side_effect = [
        {
            "results": {
                "name": "test game 2",
                "image": {
                    "thumb_url": "test_thumb_url_2"
                }
            }
        }
    ]
    mock_requests.get.side_effect = [mock_response_1, mock_response_2]

    game_data = gb_api.get_data_for_ids(["test_id_1", "test_id_2"])

    assert game_data == [
        {
            "name": "test game",
            "image_url": "test_thumb_url"
        },
        {
            "name": "test game 2",
            "image_url": "test_thumb_url_2"
        }
    ]
    print(mock_requests.get.mock_calls)
    assert mock_requests.get.mock_calls == [
        call('https://www.giantbomb.com/api/game/test_id_1', 
            headers={'user-agent': 'gb-test'}, 
            params={'api_key': 'test api key', 'format': 'json', 'field_list': 'name,image'}),
        call('https://www.giantbomb.com/api/game/test_id_2', 
            headers={'user-agent': 'gb-test'}, 
            params={'api_key': 'test api key', 'format': 'json', 'field_list': 'name,image'})]

from unittest.mock import (
    call,
    MagicMock,
    patch
)

import app

@patch('app.render_template')
def test_index(mock_render_template):
    app.index()
    assert mock_render_template.mock_calls == [
        call('search.html')
    ]


@patch('app.render_template')
@patch('gb_api.search')
@patch('app.request')
def test_display_results(mock_request, mock_search, mock_render_template):
    mock_request.args.get.side_effect = ["test"]
    mock_search.side_effect = [
        [{
            "name": "test game",
            "image_url": "test_thumb_url"
        }]
    ]

    app.search()

    assert mock_request.args.get.mock_calls == [
        call("query")
    ]

    assert mock_search.mock_calls == [
        call("test")
    ]
    assert mock_render_template.mock_calls == [
        call('search.html', results=[
            {
                "name": "test game",
                "image_url": "test_thumb_url"
            }
        ])
    ]

@patch('app.render_template')
@patch('gb_api.get_data_for_ids')
@patch('app.request')
def test_display_checkout_page(mock_request, mock_get_data, mock_render_template):
    mock_request.form.getlist.side_effect = [["game_id_1", "game_id_2"]]
    mock_get_data.side_effect = [[
        {
            "name": "test game 1",
            "image_url": "test_thumb_url_1"
        },
        {
            "name": "test game 2",
            "image_url": "test_thumb_url_2"
        }
    ]]
    
    app.checkout()

    assert mock_request.form.getlist.mock_calls == [
        call("selected")
    ]

    assert mock_get_data.mock_calls == [
        call(["game_id_1", "game_id_2"])
    ]

    assert mock_render_template.mock_calls == [
        call("checkout.html", game_info=[
            {
                "name": "test game 1",
                "image_url": "test_thumb_url_1"
            },
            {
                "name": "test game 2",
                "image_url": "test_thumb_url_2"
            }
    ])]

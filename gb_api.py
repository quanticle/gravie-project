import requests

import config

from pprint import pprint

GB_API_ENDPOINT = 'https://www.giantbomb.com/api/search/'

def search(query):
    request_params = {
        'api_key': config.API_KEY,
        'format': 'json',
        'field_list': 'name,image,guid',
        'query': query,
        'resources': 'game',
        'page': 1
    }
    request_headers = {
        'user-agent': 'gb_test'
    }
    game_data = []
    response = requests.get(GB_API_ENDPOINT, params=request_params, headers=request_headers)
    response.raise_for_status()
    response_json = response.json()
    total_results = response_json['number_of_total_results']
    num_results = response_json['number_of_page_results']
    results = response_json['results']
    game_data.extend([{'id': x['guid'], 'name': x['name'], 'image_url': x['image']['thumb_url']} for x in results])
    while num_results < total_results:
        request_params['page'] += 1
        response = requests.get(GB_API_ENDPOINT, params=request_params, headers=request_headers)
        response.raise_for_status()
        response_json = response.json()
        num_results += response_json['number_of_page_results']
        results = response_json['results']
        game_data.extend([{'id': x['guid'], 'name': x['name'], 'image_url': x['image']['thumb_url']} for x in results])
    return game_data

def get_data_for_ids(game_ids):
    pprint("Game IDs: " + repr(game_ids))
    request_url_template =  "https://www.giantbomb.com/api/game/{}"
    request_params = { 
        'api_key': config.API_KEY,
        'format': 'json',
        'field_list': 'name,image',
    }
    request_headers = {
        'user-agent': 'gb-test'
    }
    game_data = []
    for game_id in game_ids:
        response = requests.get(request_url_template.format(game_id), params=request_params, headers=request_headers)
        response.raise_for_status()
        response_json = response.json()
        pprint(response_json)
        game_data.append({'name': response_json['results']['name'], 'image_url': response_json['results']['image']['thumb_url']})
    return game_data
        
